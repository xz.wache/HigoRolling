﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLocation : MonoBehaviour {

    public GameObject track;
    private bool spawned=false;

    void Start()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !spawned)
        {
            float parentpos = transform.parent.localScale.y;
            Vector3 pos = new Vector3(transform.parent.position.x, (transform.parent.position.y - parentpos*40), transform.parent.position.z);
            Instantiate(track.transform, pos,transform.parent.rotation);
            spawned = true;
        }
    }
}
