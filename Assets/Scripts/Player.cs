﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class Player : MonoBehaviour {

    public float movementSpeed;
    public float comboTime;
    //UI elements
    public Text scoreLabel;
    public Text hightScoreLabel;
    public GameObject pauseButton;
    public GameObject rewardButton;
    public GameObject mainMenu;
   // public GameObject bonusShow;
    public MainMenu mainMenuScript;
    public WoodPool wp;
    public Vector3 playerDefaultPosition;
    /////////////////////////////

    static private int deathsCount=0;

    private int score;
    public int bonus;
    internal float nextActionTime = 0.0f;
    private float period = 1.0f;


    private bool movingLeft;
    private float rotationSpeed;

    private bool comboStart = false;
    private float Timer;




    void Start ()
    {
        playerDefaultPosition = new Vector3(0f, 0.79f, -0.05f);
        //set bonus to two
        PlayerPrefs.SetInt("Bonus", 2);

        // on start game set hight score to zero (delete)
        //PlayerPrefs.SetInt("HightScore", 0);

        //actual player score set to zero
        score = 0;
        //for moving (left or right)
        movingLeft = true;
        //on start the game move player to right side
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 18f));

        if (Advertisement.isSupported) Advertisement.Initialize("610bde8b-7beb-432a-80b1-44d60a83defd", false); 
    }

    void Update ()
    {
        transform.Translate(Vector3.down * Time.deltaTime * movementSpeed);

        if (Input.GetMouseButtonDown(0))
        {
            rotationSpeed = 0.4f;
            movingLeft = !movingLeft;
            Debug.Log(movingLeft);
        }

        if (Input.GetMouseButton(0))
        {
            rotationSpeed += 1.1f * Time.deltaTime;
        }

        if (Time.timeScale!=0)
        {
            if (movingLeft) transform.Rotate(0, 0, rotationSpeed);
            else transform.Rotate(0, 0, -rotationSpeed);
        }

        //combo timer
        if (comboStart)
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                bonus = 0;
            }
            //Debug.Log(Timer);
        }
    }

    void FixedUpdate()
    {
        //getting 2 scores per second
        if (Time.time*2 > nextActionTime)
        {
            nextActionTime += period;
            score += (int)period;
            if (score > PlayerPrefs.GetInt("HightScore", 0)) PlayerPrefs.SetInt("HightScore", score);
            hightScoreLabel.text = "Hight score: " + (PlayerPrefs.GetInt("HightScore")).ToString();
            scoreLabel.text = score.ToString("0");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle" || other.tag == "GameOverBorder")
        {
            Die();
            
            Debug.Log("DIE");
        }

        Debug.Log(other.tag);
        if (other.tag == "BonusCollider")
        {
            comboStart = true;
            Timer = comboTime;
            if (Timer > 0)
            {
                bonus += 2;
                PlayerPrefs.SetInt("Bonus", bonus);
            }
            else Timer = 0;
            score += bonus;

            //bonusShow.SetActive(true);
        }
    }


    public void Die()
    {
        //show main menu after die
        //StartCoroutine(Wait());
        
        //no rotate after restart
        rotationSpeed = 0f;

        movingLeft = true;

        gameObject.GetComponent<TrailRenderer>().time = 0f;

        deathsCount++;

        if (deathsCount < 3 || deathsCount == 4)
        {
            ShowMenu();
        }
        //long wood spawn
        wp.spawnRate = 1000000f;

        Debug.Log(deathsCount);
        //if deaths ==3 then offer bonus
        if (deathsCount == 3) { rewardButton.SetActive(true);  }
        //if deaths >= 5
        if (deathsCount >= 5)
        {
            deathsCount = 0;
            //show adverstisement
            Advertisement.Show();
            ShowMenu();
        }
        //view pause button
        pauseButton.SetActive(false);
        //activate player
        gameObject.SetActive(false);
    }

    void ShowMenu()
    {
        mainMenu.SetActive(true);
        Animator anim = mainMenu.GetComponent<Animator>();
        anim.enabled = true;
        //play menu animation
        anim.Play("MainMenuShow");
    }


    public void ShowRewardedAd()
    {
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                rewardButton.SetActive(false);
                //continue the game
                mainMenuScript.ContinueGame();

                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                rewardButton.SetActive(false);
                ShowMenu();
                //restart the game
                mainMenuScript.Restart();
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                rewardButton.SetActive(false);
                ShowMenu();
                //restart the game
                mainMenuScript.Restart();
                break;
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1f);
        mainMenu.SetActive(true);
        Animator anim = mainMenu.GetComponent<Animator>();
        anim.enabled = true;
        anim.Play("MainMenuShow");
    }
}
