﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {

    public bool isPaused = false;
    public GameObject pauseMenuUI;
    public GameObject pauseButton;
	
    void Start()
    {
        //pauseButton.SetActive(true);
        pauseMenuUI.SetActive(false);
    }

	void Update () {
    }

    public void Resume()
    {
        Debug.Log("Resumed");
        pauseButton.SetActive(true);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    public void Pause()
    {
        Debug.Log("Paused");
        pauseButton.SetActive(false);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
}
