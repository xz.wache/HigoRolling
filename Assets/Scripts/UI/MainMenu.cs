﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour {

    public GameObject menuHightScoreLabel;
    public GameObject gameplayHightScoreLabel;
    public GameObject score;
    public GameObject pauseButton;
    public Player player;
    public WoodPool wp;


    private TextMeshProUGUI textMesh;

    private Animator anim;
    // Use this for initialization
    void Start () {

        anim = gameObject.GetComponent<Animator>();

        textMesh = menuHightScoreLabel.GetComponent<TextMeshProUGUI>();
        textMesh.text = "Hight score: " + PlayerPrefs.GetInt("HightScore");
        player.movementSpeed = 0.0f;
        player.nextActionTime = 10000f;
        wp.spawnRate = 1000000000f;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            Restart();

            player.nextActionTime = 0.0f;
            player.movementSpeed = 6.0f;

            anim.Play("MainMeniDown");
            wp.spawnRate = 2.4f;

            StartCoroutine(WaitForAnimationEnd());
        }
	}

    public void Restart()
    {
        player.gameObject.SetActive(true);
        //set player on start position
        player.transform.position = player.playerDefaultPosition;
        //rotate player on default
        player.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        player.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 18f));
        //set trail to default
        player.gameObject.GetComponent<TrailRenderer>().time = 2f;
        //reset bonus score
        PlayerPrefs.SetInt("Bonus", 2);
        //set woods spawn rate to default
        wp.spawnRate = 2.4f;
        //wp.ResetPositions();
    }

    public void ContinueGame()
    {
        player.gameObject.SetActive(true);
        player.transform.position = player.playerDefaultPosition;
        player.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        player.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 18f));
        player.GetComponent<TrailRenderer>().time = 2.0f;
        wp.spawnRate = 2.4f;
    }

    IEnumerator WaitForAnimationEnd()
    {
        yield return new WaitForSeconds((float)anim.runtimeAnimatorController.animationClips[0].length-0.20f);
        gameObject.SetActive(false);
        gameplayHightScoreLabel.SetActive(true);
        pauseButton.SetActive(true);
        score.SetActive(true);
    }
}
