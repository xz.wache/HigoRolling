﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowBonus : MonoBehaviour {

    public GameObject label;
    public GameObject canvas;
    public Text bonusLabel;

    // Use this for initialization
    void Start()
    {
        label.SetActive(false);
        transform.localPosition = new Vector3(0, 0, 0);
        
    }

    // Update is called once per frame
    void Update () {
        transform.localPosition = new Vector3(0, 0, 0);
        canvas.transform.localPosition = new Vector3(0, 0, 0);
        canvas.transform.localRotation = Quaternion.Euler(new Vector3(14, 0, 0));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Show();
        }
    }


    public void Show()
    {
       label.SetActive(true);
       bonusLabel.text = "+" + PlayerPrefs.GetInt("Bonus");
    }

}
