﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWoods : MonoBehaviour {

    public GameObject spawnObject;
    public int spawnSize;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < spawnSize; i++)
        {
            GameObject obj = Instantiate(spawnObject, transform.position, transform.rotation);
            obj.transform.rotation = Quaternion.Euler(new Vector3(-98f, 0, 0));
            obj.transform.position = new Vector3(Random.Range(-5.0f, 5.0f), Random.Range(-5.0f, 5.0f), transform.position.z);
            obj.transform.parent = transform;
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
