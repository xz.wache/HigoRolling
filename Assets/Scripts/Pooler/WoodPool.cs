﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodPool : MonoBehaviour {

    public int woodPoolSize;
    public GameObject woodPrefab;
    public float spawnRate = 0.0f;

    private GameObject[] woods;
    private Vector3 objectPoolPosition;
    private float timeSinceLastSpawn;
    private int currentWood = 0;

    // Use this for initialization
    void Start () {
        objectPoolPosition = new Vector3(0, 0, +10f);
        woods = new GameObject[woodPoolSize];
        //array of woodcontainers
        for (int i = 0; i < woods.Length; i++)
        {
            //spawn woodcontainers
            woods[i] = (GameObject)Instantiate(woodPrefab, objectPoolPosition , Quaternion.identity);
        }
	}

    public void ResetPositions()
    {
        for (int i = 0; i < woods.Length; i++)
        {
            woods[i].transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 10f);
        }
    }
	
	// Update is called once per frame
	void Update () {
        timeSinceLastSpawn += Time.deltaTime;
        Transform [] childWoods;
        GameObject[] bonusLabels;

        if (timeSinceLastSpawn >= spawnRate)
        {
            timeSinceLastSpawn = 0;

            //move current woodcontainer on next position
            woods[currentWood].transform.position = new Vector3(transform.position.x, transform.position.y - 19.0f, transform.position.z);

            //for all bonus labels set active to false, turn off for the function "ShowBonus"
            bonusLabels = GameObject.FindGameObjectsWithTag("Bonus");
            for (int i = 0; i < bonusLabels.Length; i++)
            {
                bonusLabels[i].SetActive(false);
            }

            //get woods from current woodcontainer
            childWoods = woods[currentWood].GetComponentsInChildren<Transform>();
            //for all woods
            for (int i = 0; i < childWoods.Length; i++)
            {
                //randrom new position
                childWoods[i].transform.position = new Vector3(childWoods[i].transform.position.x + Random.Range(-3.0f, 3.0f), childWoods[i].transform.position.y + Random.Range(-3.0f, 3.0f), childWoods[i].transform.position.z);
            }
            //choose the following woodcontainer
            currentWood++;

            if (currentWood >= woods.Length)
            {
                currentWood = 0;
            }
        }
	}
}
